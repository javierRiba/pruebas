package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.BeforeClass;

import static java.lang.Math.*;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class FacturaTest {
	static Factura facturaPrueba = new Factura();
	static GestorContabilidad gestor = new GestorContabilidad();
	/* 
	 * Generar factura prueba y gestor 
	 */
	@BeforeClass
	public static void prepararClasePuebas() {
		facturaPrueba = new Factura();
		gestor = new GestorContabilidad();
	}
	
	/*
	 * Calcular precio producto
	 */
	@Test
	public void testCalcularPrecioProducto() {
		
		facturaPrueba.setCantidad(3);
		facturaPrueba.setPrecioUnidad(2.5F);
		
		double esperado = 7.5F;
		double actual =  facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual , 0.0);
	}
	/*
	 * calcular precio con cnatidad 0
	 */
	@Test
	public void testCalcularPrecioCantidadCero() {
		
		facturaPrueba.setCantidad(0);
		facturaPrueba.setPrecioUnidad(2.5F);
		
		double esperado =0;
		double actual =  facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual , 0.0);
	}
	/*
	 * calcular precio con precio negativo
	 */
	@Test
	public void testCalcularPrecioConPrecioNegativo() {

		facturaPrueba.setCantidad(2);
		facturaPrueba.setPrecioUnidad(-1.5F);
		
		double esperado = -3F;
		double actual =  facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual , 0.0);
	}
	/*
	 * Facturacion en un a�o
	 */
	@Test
	public void FacturaAnual(){
		
		Factura unafactura= new Factura(4, 1,"2018");
		Factura unafactura1= new Factura(5, 7,"2018");
				
		gestor.getListaFacturas().add(unafactura);
		gestor.getListaFacturas().add(unafactura1);
		float resultado;
		resultado=gestor.calcularFacAnual("2018");
		float esperado=17;
		assertEquals(esperado, resultado, 0);
	}
	/*
	 * buscar factura
	 */
	@Test
	public void BuscarFactura(){
		Factura Actual= new Factura();
		Actual=gestor.buscarFactura("1243");
		
		assertNull(Actual);
	}
	/*
	 * Buscamos factura existente
	 */
	@Test
	public void buscarFacturaExistente(){
		Factura Actual= new Factura();
		Actual.setCodigofactura("123456");
		Factura esperada = new Factura();
		gestor.getListaFacturas().add(Actual);
		esperada=gestor.buscarFactura("123456");
		
		assertEquals(Actual, esperada);
		
		
	}
	/*
	 * Factura mas cara
	 */
	@Test
	public void facturaMasCara(){
		
	Factura esperada= gestor.facturaMascara();
	Factura unafactura1=gestor.getListaFacturas().get(1);
	assertEquals(unafactura1, esperada);
	}
	/*
	 * Crear 2 facturas
	 */
	@BeforeClass
	public static void generarFacturas(){
		Cliente uncliente= new Cliente("Montesori", "Jose", LocalDate.now());
		
		Factura factura= new Factura(4, 1);
		Factura factura1= new Factura(5, 7);
				
		factura.setCliente(uncliente);
		factura1.setCliente(uncliente);
			
		gestor.getListaFacturas().add(factura);
		gestor.getListaFacturas().add(factura1);
	}
	/*
	 * Contador facturas de un cliente
	 */
		@Test
		public void contarFacturasexistentes(){
			int esperado=3;
			int resultado=gestor.cantidadFacturasCliente("Montesori");
			
			assertEquals(esperado, resultado);
		}
}
