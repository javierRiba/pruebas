package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class ClientesTest {
	Cliente actual;
	Cliente esperado;
	static GestorContabilidad gestor = new GestorContabilidad();
	@BeforeClass
	public static void prepararClasePuebas() {
		
		gestor = new GestorContabilidad();
	}
	/*
	 * A�adir clientes
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		 gestor = new GestorContabilidad();
		Cliente busuqedacliente = new Cliente("12345A", "Javier", LocalDate.parse("1800-10-15"));
		Cliente busquedaclietne1 = new Cliente("123456Y", "Jose", LocalDate.parse("2018-01-22"));
			gestor.getListaCliente().add(busuqedacliente);
		gestor.getListaCliente().add(busquedaclietne1);
		
	}
	/*
	 * Eliminar cliente existente
	 */
		@Test
		public void eliminarClienteexistente(){
			Cliente clientebuscado = new Cliente("sdfsdf", "ramos", LocalDate.now());
			gestor.getListaCliente().add(clientebuscado);
			
			gestor.eliminarCliente("sdfsdf");
			boolean err=gestor.getListaCliente().contains(clientebuscado);
			assertFalse(err);
		}
		/*	
		 * eliminar clietne no exixtente
		 */
		@Test
	public void eliminarClienteinexistente(){
		GestorContabilidad gestor = new GestorContabilidad();
		Cliente busuqedacliente = new Cliente("12345A", "Javier", LocalDate.parse("1800-10-15"));
		Cliente busquedaclietne1 = new Cliente("123456Y", "Jose", LocalDate.parse("2018-01-22"));
			gestor.getListaCliente().add(busuqedacliente);
		gestor.getListaCliente().add(busquedaclietne1);
		
		try{
		gestor.eliminarCliente("4896OI4");
		fail(" nullpointerException");
		}catch (NullPointerException e) {
					}
	}
/*
 * buscar cliente sin cliente
 */
	@Test
	void testBuscarClienteInexistenteSinClientes() {
		GestorContabilidad gestorPruebas = new GestorContabilidad();

		String dni = "446575";

		Cliente actual = gestorPruebas.buscarcliente(dni);
		assertNull(actual);
	}
/*
 * buscar cliente inexxitente con clietnes
 */
	@Test
	public void testBuscarClienteInexistenteConClientes() {
		GestorContabilidad gestorPruebas = new GestorContabilidad();

		Cliente esperado = new Cliente("1234J", "Momath", LocalDate.now());

		gestorPruebas.getListaCliente();

		Cliente actual = gestorPruebas.buscarcliente("34F");

		assertNull(actual);
	}

	GestorContabilidad gestorPruebas;

	/*
	 * buscar cliente exixtente
	 */

	@Test
	public void testBuscarClienteExistente() {
		esperado = new Cliente("1234J", "javi", LocalDate.now());
		;
		gestorPruebas.getListaCliente().add(esperado);
		actual = gestorPruebas.buscarcliente("1234J");
		assertSame(esperado, actual);
	}
/*
 * buscar clietne inexixtente
 */
	@Test
	public void testBuscarClienteInexistente() {
		String dni = "64F";

		actual = gestorPruebas.buscarcliente("dni");
		assertNull(actual);
	}
/*
 * buscar clietne habiendo cliente
 */
	@Test
	public void testBuscarClienteHabiendoCLiente() {
		esperado = new Cliente("4567F", "Francisco", LocalDate.parse(""));
		;
		String dniABuscar = "1324567L";
		esperado = new Cliente(dniABuscar, "Pedro", LocalDate.parse("11995-07-02"));
		gestorPruebas.getListaCliente().add(esperado);
		actual = gestorPruebas.buscarcliente(dniABuscar);
		assertSame(esperado, actual);
	}
	/*
	 *  cliente mas antiguo
	 */
	@Test
	public void textClienteMasAntiguo() throws Exception{
		GestorContabilidad gestorpruebas = new GestorContabilidad();
		Cliente clientebuscado = new Cliente("sdfsdf", "ramos", LocalDate.parse("2001-03-13"));
		Cliente clientebuscado2 = new Cliente("464564", "ruan", LocalDate.parse("1998-03-13"));
		Cliente clientebuscado3 = new Cliente("1234F", "Juan", LocalDate.now());
		gestor.getListaCliente().add(clientebuscado);
		gestor.getListaCliente().add(clientebuscado2);
		gestor.getListaCliente().add(clientebuscado3);
		Cliente resultado;
		resultado=gestor.clienteMasAntiguo();
		assertEquals(clientebuscado2, resultado);
	}
}
