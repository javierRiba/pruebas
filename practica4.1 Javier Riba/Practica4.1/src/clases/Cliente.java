package clases;

import java.time.LocalDate;

public class Cliente {
	private String dni;
	String nombre;
	private LocalDate fechaAlta;
	
	public Cliente(String dni , String nombre , LocalDate fecha) {
		this.dni = dni;
		this.nombre = nombre;
		this.fechaAlta = fechaAlta ;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public LocalDate getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

}
