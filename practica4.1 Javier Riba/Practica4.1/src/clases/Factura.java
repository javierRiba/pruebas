package clases;

import java.time.LocalDate;

public class Factura {
	private String codigofactura;
	private LocalDate fecha;
	private String nombreProducto;
	private double precioUnidad;
	private int cantidad;
	private  Cliente Cliente;
	
	public Factura(String codigofactura, LocalDate fecha, String nombreProducto, double precioUnidad, int cantidad,
			clases.Cliente cliente) {
		super();
		this.codigofactura = codigofactura;
		this.fecha = fecha;
		this.nombreProducto = nombreProducto;
		this.precioUnidad = precioUnidad;
		this.cantidad = cantidad;
		Cliente = cliente;
		}
	public Factura() {
		
	}
	public Factura(int i, int j, String string) {
		// TODO Auto-generated constructor stub
	}
	
	public Factura(int i, int j) {
		// TODO Auto-generated constructor stub
	}
	public String getCodigofactura() {
		return codigofactura;
	}

	public void setCodigofactura(String codigofactura) {
		this.codigofactura = codigofactura;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public double getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(double precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Cliente getCliente() {
		return Cliente;
	}

	public void setCliente(Cliente cliente) {
		Cliente = cliente;
	}
	public double calcularPrecioTotal() {
	return cantidad * precioUnidad;
	}
	public Factura buscarFactura(String string) {
		// TODO Auto-generated method stub
		return null;
	}
	public Factura facturaMascara() {
		// TODO Auto-generated method stub
		return null;
	}
	}
	
