package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;

class TestAltaClientes {
	

	static	GestorContabilidad gestorPruebas;
	Cliente actual;
	
	@BeforeClass
	public static void inicializarClase(){
		gestorPruebas = new GestorContabilidad();
	}

	

	//damos de alta a un cliente
	@Test
	public void testAltaPrimerCliente() {
		Cliente nuevoCliente=new Cliente("123456","PABLO", LocalDate.parse("2010-05-10"));
		
		gestorPruebas.altaCliente(nuevoCliente);
		
		boolean actual=gestorPruebas.getListaClientes().contains(nuevoCliente);
		
		assertTrue(actual);
	}
	
	//damos de alta a varios clientes
	@Test
	public void testAltaVariosClientes() {
		Cliente unCliente=new Cliente("7474J","ANA", LocalDate.parse("2016-07-08"));
		gestorPruebas.altaCliente(unCliente);
		
		Cliente segundoCliente=new Cliente("8563G","JOSE", LocalDate.parse("2014-04-09"));
		gestorPruebas.altaCliente(unCliente);
		
		boolean actual=gestorPruebas.getListaClientes().contains(unCliente);
		boolean actual2=gestorPruebas.getListaClientes().contains(segundoCliente);
		assertTrue(actual);
		assertTrue(actual2);
	}
	
	//damos de alta un cliente con  dni repetido
	@Test
	public void testAltaClienteDniRepetido() {
		Cliente nuevoCliente=new Cliente("123456","PABLO", LocalDate.parse("2012-5-30"));
		
		gestorPruebas.altaCliente(nuevoCliente);
		
		Cliente otroCliente=new Cliente("123456","LUIS", LocalDate.parse("2001-1-1"));
		
		boolean actual=gestorPruebas.getListaClientes().contains(nuevoCliente);
		
		assertTrue(actual);
	}
}
