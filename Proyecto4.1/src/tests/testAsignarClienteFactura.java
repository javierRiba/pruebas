package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class testAsignarClienteFactura {

	static GestorContabilidad gestorPruebas;
	static Cliente unCliente;
	static Factura unaFactura;
	
	@BeforeAll
	public static void prepararClasePruebas() {
		gestorPruebas = new GestorContabilidad();
		unCliente = new Cliente("1234V","Alex",LocalDate.now());
		unaFactura = new Factura("12345",LocalDate.now(),"Patatas",5,2,null);
	}

	//asignamos un cliente a una factura
	@Test
	public void introducircliente(){
	GestorContabilidad gestor= new GestorContabilidad();
	Cliente cliente1 = new Cliente("34567F","Maria", LocalDate.now());
	gestor.AsignarFacturaACliente("34567F", "565");
		
}
	/*
	 * comprobar si se ha  añadido el cliente a la factura
	 */
	@Test
	public void añadirClienteAFactura() {
	gestorPruebas.asignarClienteAFactura(unCliente.getDni(), unaFactura.getCodigoFactura());
	Factura facturaActual= gestorPruebas.buscarFactura(unaFactura.getCodigoFactura());
	boolean actual=facturaActual.getCliente().equals(unCliente);
	assertTrue(actual);
	}
	/*
	 * comprobar que se ha añadido un cliente inexxitente a una factura
	 */
	@Test
	public void añadirClienteInexistenteAFactura() {
	gestorPruebas.getListaFacturas().clear();
	gestorPruebas.asignarClienteAFactura("123", unaFactura.getCodigoFactura());
	Factura facturaActual= gestorPruebas.buscarFactura(unaFactura.getCodigoFactura());
	boolean actual=facturaActual.getCliente().equals(unCliente);
	assertFalse(actual);
	}
}
