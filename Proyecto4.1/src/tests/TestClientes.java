package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestClientes {
	static	GestorContabilidad gestorPruebas;
	Cliente actual;
	
	@BeforeClass
	public static void inicializarClase(){
		gestorPruebas = new GestorContabilidad();
		
	}
	
	//busco un cliente que no existe sin lista de clientes
	@Test
	public void testBuscarClienteInexistenteSinClientes() {
		
		gestorPruebas.getListaClientes().add(actual);
		String dni = "2345234";
		
		Cliente actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}
	
	// busco cliente que existe 
	@Test
	public void testBuscarClienteExistente(){
		Cliente esperado = new Cliente("1234F", "FERNANDO", LocalDate.now());
		gestorPruebas.getListaClientes().add(esperado);
		actual = gestorPruebas.buscarCliente("1234F");
		assertSame(esperado, actual);
	}
	// añadir cliente existente
	@Test
	public void testAñadirClienteExistente() {
		Cliente esperado = new Cliente("1234F", "FERNANDO", LocalDate.now());
		gestorPruebas.altaCliente(esperado);
		boolean actual=gestorPruebas.getListaClientes().contains(esperado);
		
		assertFalse(actual);
	}
	
	//busco un cliente que no está en la lista de clientes
	@Test
	public void testBuscarClienteInexistenteConClientes(){	
		String dni = "64F";
		//Busco un objeto que no exista, el resultado debe ser null
		actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}
	
	//buscamos  un cliente dentro de una lista con varios clientes
	@Test
	public void testBuscarClienteHabiendoVariosClientes(){
		Cliente esperado = new Cliente("34567F", "MARIA", LocalDate.parse("1990-05-04"));
		gestorPruebas.getListaClientes().add(esperado);
		String dniABuscar = "1234678L";
		esperado = new Cliente(dniABuscar, "PEDRO", LocalDate.parse("1995-07-02"));
		gestorPruebas.getListaClientes().add(esperado);
		
		actual = gestorPruebas.buscarCliente(dniABuscar);
		
		assertSame(esperado, actual);
	}
	
	//buscamos el cliente más antiguo
	@Test
	public void testClienteMasAntiguo() {
		Cliente cliente1 = new Cliente("34567F", "MARIA", LocalDate.parse("1990-05-04"));
		Cliente cliente2= new Cliente("1234F", "JAVI", LocalDate.now());
		gestorPruebas.getListaClientes().add(cliente1);
		gestorPruebas.getListaClientes().add(cliente2);
		Cliente actual=gestorPruebas.clienteMasAntiguo();
		assertEquals(cliente1, actual);
	}
	
	//eliminar un cliente existente
	@Test 
	public void eliminarClienteExistente() {
		Cliente esperado = new Cliente("34567F", "MARIA", LocalDate.parse("1990-05-04"));
		gestorPruebas.getListaClientes().add(esperado);
		gestorPruebas.getListaClientes().clear();
		gestorPruebas.eliminarCliente("34567F");
		boolean encontrado=gestorPruebas.getListaClientes().contains(esperado);
		assertFalse(encontrado);
	}
	// eliminar cliente inexixtente
	@Test 
	public void eliminarClienteInexistente() {
		Cliente esperado = new Cliente("34567F", "MARIA", LocalDate.parse("1990-05-04"));
		boolean encontrado=gestorPruebas.getListaClientes().contains(esperado);
		assertFalse(encontrado);
	}
	
}
